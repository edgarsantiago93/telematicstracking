<?php

namespace App\Http\Controllers;

use App\TrackingInstance;
use Illuminate\Http\Request;

class TrackingController extends Controller
{
    //


    public function saveIncomingData(Request $r)
    {
        $tInstance = new TrackingInstance();
        $tInstance->payload_data = $r;
        $tInstance->save();
        return response('', 200);
    }
}
